provider "yandex" {
}

data "yandex_compute_image" "container-optimized-image" {
  family    = "container-optimized-image"
}

data "yandex_vpc_network" "network" {
  name = "default"
}


resource "yandex_compute_instance_group" "yandex-gitops-demo" {
  name               = "demo-ig-gitops"
  service_account_id = var.yandex_sa_id
  instance_template {
    platform_id = "standard-v1"
    resources {
      memory = 1
      cores  = 1
    }
    boot_disk {
      mode = "READ_WRITE"
      initialize_params {
        image_id = data.yandex_compute_image.container-optimized-image.id
      }
    }
    network_interface {
      network_id = data.yandex_vpc_network.network.network_id
    }

    service_account_id = var.yandex_sa_id
  }

  scale_policy {
    fixed_scale {
      size = 1
    }
  }

  allocation_policy {
    zones = ["ru-central1-a"]
  }

  deploy_policy {
    max_unavailable = 2
    max_creating    = 2
    max_expansion   = 2
    max_deleting    = 2
  }
}
